﻿using System;
using UnityEngine;

[Serializable]
public class Wave
{
    public string name;
    public Enemy enemy;
    public int count;
    public float rate;
}
